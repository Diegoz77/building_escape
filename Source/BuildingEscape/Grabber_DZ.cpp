// Copyright Diego Zambrano Guerrero 2018

#include "Grabber_DZ.h"
#include "Engine/World.h"
#include "DrawDebugHelpers.h" 
#include "Components/InputComponent.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"
#include "Components/PrimitiveComponent.h"

#define OUT

// Sets default values for this component's properties
UGrabber_DZ::UGrabber_DZ()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UGrabber_DZ::BeginPlay()
{
	Super::BeginPlay();

	FindPhysicsHandleComponent();

	SetupInputComponent();
	


	
}



void UGrabber_DZ::Grab()
{
	//Line trace and try to reach any actors with physics body collision channel set
	auto HitResult = GetFirstPhysicsBodyInReach();
	auto ComponentToGrab = HitResult.GetComponent(); // gets the mesh in our case
	auto ActorHit = HitResult.GetActor();

	// If we hit something then attach a physics handle
	if (ActorHit)
	{
		PhysicsHandle->GrabComponentAtLocationWithRotation(
		ComponentToGrab,
		NAME_None, // no bones needed
		ComponentToGrab->GetOwner()->GetActorLocation(),
		FRotator(0)
		);
	}
}

void UGrabber_DZ::Release()
{
		//release physics handle
		PhysicsHandle->ReleaseComponent();
}

void UGrabber_DZ::FindPhysicsHandleComponent()
{
	/// look for attached physics handle
	PhysicsHandle = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();
	if (PhysicsHandle == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("%s missing physycs handle component"), *GetOwner()->GetName())
	}
}

void UGrabber_DZ::SetupInputComponent()
{
	/// look for attached Input Component (only appears at runtime)

	InputComponent = GetOwner()->FindComponentByClass<UInputComponent>();
	if (InputComponent)
	{
		UE_LOG(LogTemp, Warning, TEXT("Inpiut Component found")) // Input handle found
		/// Bind the input axis
		InputComponent->BindAction("Grab", IE_Pressed, this, &UGrabber_DZ::Grab);
		InputComponent->BindAction("Grab", IE_Released, this, &UGrabber_DZ::Release);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("%s missing input handle component"), *GetOwner()->GetName())
	}
}



// Called every frame
void UGrabber_DZ::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	
	// if physics handle is attached 
	if (PhysicsHandle->GrabbedComponent)
	{
		//move the object taht we are holding
		PhysicsHandle->SetTargetLocation(GetReachLineEnd());
	}
		
	
}

const FHitResult UGrabber_DZ::GetFirstPhysicsBodyInReach()
{
	/// Line trace (AKA Ray-cast) out to reach distance
	FHitResult LineTraceHit;
	/// Setup query parameters
	FCollisionQueryParams TraceParameters(FName(TEXT("")), false, GetOwner());

	GetWorld()->LineTraceSingleByObjectType(
		OUT LineTraceHit,
		GetReachLineStart(),
		GetReachLineEnd(),
		FCollisionObjectQueryParams(ECollisionChannel::ECC_PhysicsBody), TraceParameters
	);

	return LineTraceHit;
}
FVector UGrabber_DZ::GetReachLineStart()
{
	FVector PlayerViewPointLocation;
	FRotator PlayerViewPointRotation;
	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(
		OUT PlayerViewPointLocation,
		OUT PlayerViewPointRotation
	);
	return PlayerViewPointLocation;
}

FVector UGrabber_DZ::GetReachLineEnd()
{
	FVector PlayerViewPointLocation;
	FRotator PlayerViewPointRotation;
	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(
		OUT PlayerViewPointLocation,
		OUT PlayerViewPointRotation
	);
	return PlayerViewPointLocation + PlayerViewPointRotation.Vector() * Reach;
}

 